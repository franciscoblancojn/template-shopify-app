import { Heading, Page } from "@shopify/polaris";
import { useEffect, useState } from "react";
import getConfig from 'next/config'

import Template from "@/components/template"

import APITEMPLATE from "@/api/template"

const API = APITEMPLATE()

const Index = () => {
  const [env, setEnv] = useState({})

  const loadEnv = () => {
		const {publicRuntimeConfig} = getConfig()
		const {ENV} = publicRuntimeConfig
    setEnv(ENV)
  }

  const load = async () => {
    loadEnv()
    console.log( await API.getAllPost());
  }
  useEffect(() => {
    load()
  }, [])

  return (
    <Page>
      <Heading>Shopify app with Node and React 🎉</Heading>
      <Template>Template</Template>
      {JSON.stringify(env)}
    </Page>
  )
}

export default Index;
