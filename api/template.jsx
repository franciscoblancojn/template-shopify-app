const APITEMPLATE = () => {
    const URLAPI = "https://jsonplaceholder.typicode.com"

    const request = async ({ method = "GET", json = {}, header = {} , url = ""}) => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        Object.keys(header).forEach(key => {
            myHeaders.append(key, header[key]);
        });
        var requestOptions = {
            method,
            headers: myHeaders,
            redirect: 'follow'
        };
        if(method.toUpperCase() !="GET"){
            requestOptions.body = JSON.stringify(json)
        }
        try {
            const response = await fetch(URLAPI + url, requestOptions)
            const result = await response.json()
            return {
                type : "ok",
                result
            }
        } catch (error) {
            return {
                type : "error",
                error
            }
        }
    }
    const getAllPost = async () => {
        return await request({
            url : "/posts"
        })
    }
    return {
        getAllPost
    }
}
export default APITEMPLATE
