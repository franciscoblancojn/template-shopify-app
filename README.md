# Template Shopify App
It is an app in NodeJs and Nextjs for Shopify

## Installation
1. Create App in Shopify Partners
2. Clone the repositore 
```bash
git clone https://gitlab.com/franciscoblancojn/template-shopify-app.git
```
3. Create field .env copy .env.example 
4. Add keys
```bash
SHOPIFY_API_KEY="YOUR_SHOPIFY_API_KEY"
SHOPIFY_API_SECRET="YOUR_SHOPIFY_SECRET"
HOST="YOUR_TUNNEL_URL"
SHOP="my-shop-name.myshopify.com"
SCOPES=read_products
PORT=3001
```
5. Delete folder .git
6. Install dependencies
```bash
npm i
```

## Usage
For testing your app use the comand
```bash
shopify node serve
```

## Backend
The file of backend is
```rute
/server/saveToken.js
```
In this file, you can save token of appShopify for use [API Shopify](https://shopify.dev/api)

## Frontend
The files of frontend are
```rute
/page/*
```
In this file, you can modify the pages of app.
Language [NextJs](hhttps://nextjs.org/docs/getting-started)
```rute
/api/*
```
In this file, you can modify the connection with api of app.
Language Javascript
```rute
/components/*
```
In this file, you can modify the components used in pages of app.
Language [NextJs](hhttps://nextjs.org/docs/getting-started)
```rute
/styles/*
```
In this file, you can modify the styles of app.
Language [SASS](https://sass-lang.com/documentation)

## Documentation
[Apps Shopify NodeJs](https://shopify.dev/apps/)

[Api Shopify NodeJs](https://shopify.dev/api/)


## Author
[Francisco Blanco](https://franciscoblanco.vercel.app/)

[Email](mailto:blancofrancisco34@gmail.com)

[Gitlab](https://gitlab.com/franciscoblancojn/)