const Index = ({className="",children="",id="",data}) => {
    return (
        <div id={id} className={`${className}`} {...data}>
            {children}
        </div>
    )
}
export default Index